# gadoxetate_model_selection_framework

This project contains code specific to our work modelling the tracer-kinetics of the contrast-agent gadoxetate in DCE-MRI of the liver.

This work is currently under submission to MRM.

The code contained was used to produce our analysis. Because this involves processing raw image data that we are not able to make public at this time, not all the code in the project will run, however it is include here for transparency.

The code is organised in to 3 folders, one for the Monte-Carlo experiments (this should run), and one each used to process the two in-vivo datasets.

The code depends on three other public projects from our public group:
- https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx
- https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_python
- https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_matlab
