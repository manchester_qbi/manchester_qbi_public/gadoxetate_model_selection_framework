function [] = milano_monte_carlo(varargin)
%MILANO_MONTE_CARLO *Insert a one line summary here*
%   [] = milano_monte_carlo(varargin)
%
% MILANO_MONTE_CARLO uses the U_PACKARGS interface function
% and so arguments to the function may be passed as name-value pairs
% or as a struct with fields with corresponding names. The field names
% are defined as:
%
% Mandatory Arguments:
%
% Optional Arguments:
%
% Outputs:
%
% Example:
%
% Notes:
%
% See also:
%
% Created: 06-Jan-2020
% Author: Michael Berks 
% Email : michael.berks@manchester.ac.uk 
% Phone : +44 (0)161 275 7669 
% Copyright: (C) University of Manchester 

% Unpack the arguments:
args = u_packargs(varargin, 0, ...
    'experiment_root', 'Q:/data/MB/milano_primovist/experiments/monte_carlo',...
    'results_dir', [],...
    'n_pts', 1e3,...
    'n_t', 100,...
    'injection_image', 0,...
    'hct', 0.42,...
    'dose', 0.025,...
    'make_param_data', 0,...
    'make_mc_data', 0,...
    'make_ct_data', 0,...
    'use_real_aif', 0,...
    'use_real_noise', 0,...
    'study_dir', 'C:/isbe/qbi/data/milano_primovist',...
    'subject_names', [],...
    'dyn_conc_dir', [],...
    'model_conc_dir', [],...
    'aif_dir', [],...
    'roi_mask', [],...
    'sigma', 25,...
    'do_fitting', 0,...
    'do_tau_fit', 0,...
    'do_analysis',0,...
    'do_plotting', 0,...
    'fig_dir', []);
clear varargin;

% Experiments to run

%%
% Model forms
% i) 
results_dir = [args.experiment_root '/'...
    args.results_dir '/'];
create_folder(results_dir);
n_pts = args.n_pts;
n_pts2 = n_pts*2;
regimes = {'active', 'cxm'};
    
if ~isempty(args.fig_dir)
    create_folder(args.fig_dir);
end

if ~exist([results_dir 'param_data.mat'], 'file') || args.make_param_data
    analysis_dir = [args.study_dir '/experiments/patients_hct/'];
    roi_analysis = u_load([analysis_dir 'roi_analysis.mat']); 

    param_data = [];
    param_data.active.params = ...
        {'F_p', 'v_ecs', 'k_i', 'k_ef', 'f_a', 'tau_a'};
    param_data.cxm.params = ...
        {'F_p', 'PS', 'v_e', 'v_p', 'f_a', 'tau_a'};
    roi.active.params = {'F_p', 've', 'k_i', 'k_ef', 'fa', 'aoffset'};
    roi.cxm.params = {'F_p', 'PS', 've', 'vp', 'fa', 'aoffset'};

    %Make bins for each parameter
    for reg = regimes
        for i_p = 1:6
            param = param_data.(reg{1}).params{i_p};
            roi_param = roi.(reg{1}).params{i_p};
            param_data.(reg{1}).(param).bins = get_bins(roi_param, reg{1});
            param_data.(reg{1}).(param).counts = zeros(...
                size(param_data.(reg{1}).(param).bins));
        end
    end

    %Now loop through the patients filling in the bins to estimate param
    %distribution
    for i_sub = 1:10
        update_distributions(i_sub);
    end

    %Finally, loop back over the params, normalise, and display
    for reg = regimes
        figure;
        params = param_data.(reg{1}).params;
        for i_param = 1:length(params)
            %Adjust first and last params
            param = params(i_param);
            param_data.(reg{1}).(param{1}).counts_orig = ...
                param_data.(reg{1}).(param{1}).counts;
            
            param_data.(reg{1}).(param{1}).counts(1) = ...
                param_data.(reg{1}).(param{1}).counts(2);
            param_data.(reg{1}).(param{1}).counts(end) = ...
                param_data.(reg{1}).(param{1}).counts(end-1);

            param_data.(reg{1}).(param{1}).dist = ...
                 param_data.(reg{1}).(param{1}).counts / ...
                 sum( param_data.(reg{1}).(param{1}).counts);

            resampled_dist = sample_from_param_dist(...
                param{1}, reg{1}, 1e4);
            resampled_counts = hist(resampled_dist, ...
                param_data.(reg{1}).(param{1}).bins);

            subplot(2,3,i_param);
            bar(...
                param_data.(reg{1}).(param{1}).bins,...
                resampled_counts);

            title([reg{1} param{1}]);
        end
        if ~isempty(args.fig_dir)
            saveas(gcf, sprintf('%s/param_dist_%s.png',...
                args.fig_dir, reg{1}));
        end
    end
    save([results_dir 'param_data.mat'], 'param_data');
else
    load([results_dir 'param_data.mat']);
end
%%
if ~exist([results_dir 'mc_data.mat'], 'file') || args.make_mc_data
    mc_data = [];
    for reg = regimes
        for param = param_data.(reg{1}).params(1:6)
            mc_data.(reg{1}).(param{1}) =...
                sample_from_param_dist(param{1}, reg{1}, n_pts);
        end
    end

    %%
    %Active forms of regime-free model params
    [...
       mc_data.active.F_pos, mc_data.active.F_neg, ...
       mc_data.active.K_pos, mc_data.active.K_neg] = ...
        active_params_phys_to_model(...
         mc_data.active.F_p, mc_data.active.v_ecs, ...
         mc_data.active.k_i, mc_data.active.k_ef);
    [...
        mc_data.active.cxm.F_p,...
        mc_data.active.cxm.PS,...
        mc_data.active.cxm.v_e,...
        mc_data.active.cxm.v_p] = two_cx_params_model_to_phys(...
            mc_data.active.F_pos, ...
            mc_data.active.F_neg, ...
            mc_data.active.K_pos, ...
            mc_data.active.K_neg);

    %Cxm forms
    [...
       mc_data.cxm.F_pos, mc_data.cxm.F_neg, ...
       mc_data.cxm.K_pos, mc_data.cxm.K_neg] = ...
        two_cx_params_phys_to_model(...
        mc_data.cxm.F_p, mc_data.cxm.PS,...
        mc_data.cxm.v_e, mc_data.cxm.v_p);
    
    mc_data.active.active_mask = make_active_mask(...
        mc_data.active.cxm, args.hct);
    mc_data.cxm.active_mask = make_active_mask(...
        mc_data.cxm, args.hct);
    
    save([results_dir 'mc_data.mat'], 'mc_data');
else
    load([results_dir 'mc_data.mat'], 'mc_data');
end

if args.do_fitting
     
    %Concatenate params from each regime
    F_pos = [mc_data.active.F_pos(:); mc_data.cxm.F_pos(:)];
    F_neg = [mc_data.active.F_neg(:); mc_data.cxm.F_neg(:)];
    K_pos = [mc_data.active.K_pos(:); mc_data.cxm.K_pos(:)];
    K_neg = [mc_data.active.K_neg(:); mc_data.cxm.K_neg(:)];
    f_a = [mc_data.active.f_a(:); mc_data.cxm.f_a(:)];
    tau_a = [mc_data.active.tau_a(:); mc_data.cxm.tau_a(:)];

    fixed_params{1} = [1 3 7];  % i22
    fixed_params{2} = [3 7];    % i23
    fixed_params{3} = [7]; %#ok % i24

    init_params{1} = [0.0,0.2,0.0,4.0,0.25,0.025,0.0];
    init_params{2} = [0.2,0.2,0.0,4.0,0.25,0.025,0.0];
    init_params{3} = [0.2,0.2,0.5,4.0,0.25,0.025,0.0];

    %Set up times and IFs
    injection_img = args.injection_image;
    if args.use_real_aif
        aif_name = [args.study_dir '/' args.aif_dir '/group_aif.txt'];
        aif = load_aif(aif_name, 0.42);
        t = aif(1:args.n_t,1);
        Ca_t = aif(1:args.n_t,2);
        n_t = length(t);

    else
        n_t = args.n_t;
        t = linspace(0,args.end_time,n_t);
        Ca_t = population_aif(t, injection_image, 'dose', args.dose);
        aif_name = [];
    end
    Cv_t = compute_PIF(Ca_t, [], t); 

    if ~exist(sprintf('%sC_t.mat', results_dir), 'file') || args.make_ct_data
        %Make concentration time-series
        [C_t] = diirf_model(...
            F_pos, F_neg, K_pos, K_neg, ... 
            f_a, tau_a, Ca_t, Cv_t, t, 0);

        if args.use_real_noise
                noise = get_noise_time_series();
                C_t_n = C_t + noise;
        else
            %Set constanst we need to convert between signal and concentration
            FA = 20;
            TR = 2.4;
            relax_coeff = 14.0;
            T1_0 = 1000;
            M0 = 100;

            %Convert to signal
            [S_t] = ...
                concentration_to_signal(C_t, FA, TR, T1_0, M0, ...
                relax_coeff, injection_img-1);

            %%Add rician noise
            S_t_n = add_rician_noise(S_t, args.sigma);

            %Convert back to concentration
            [C_t_n] = ...
                signal_to_concentration(S_t_n, FA, TR, T1_0, ...
                relax_coeff, injection_img-1, M0);
            save(...
                sprintf('%sS_t.mat', results_dir),...
                'S_t', 'S_t_n');
        end

        save(sprintf('%sC_t.mat', results_dir),...
            'C_t_n', 'C_t');
    else
        load(sprintf('%sC_t.mat', results_dir),...
            'C_t_n');
    end

    model_params = zeros(n_pts2, 7, 3);
    model_fit = zeros(n_pts2, 3);
    C_t_m = zeros(n_pts2, n_t, 3);
    
    if args.do_tau_fit
        if ~exist([results_dir 'tau_fit.mat'], 'file')
            madym_args = [];
            madym_args.input_Ct = 1;
            madym_args.M0_ratio = 1;
            madym_args.dyn_times = t(1:args.do_tau_fit);
            madym_args.hct = args.hct;
            madym_args.dose = args.dose;
            madym_args.aifName = aif_name;
            madym_args.first_image = 0;
            madym_args.injection_image = injection_img;
            madym_args.init_params = init_params{2};
            madym_args.fixed_params = fixed_params{2};
            madym_args.IAUC_times = 30;
                
            [local_fit] = ...
                madym_taua_grid_search(C_t_n(:,1:args.do_tau_fit),...
                madym_args);
            save([results_dir 'tau_fit.mat'], 'local_fit');
        else
            load([results_dir 'tau_fit.mat'], 'local_fit');
        end
    end

    for i_init = 1:3
        
        if args.do_tau_fit
            init_params{i_init} = repmat(init_params{i_init}, n_pts2, 1);
            init_params{i_init}(:,6) = local_fit.model_params(:,6);
            fixed_params{i_init} = sort([fixed_params{i_init} 6]);
        end

        fprintf('Fitting DIBEM %d\n', i_init);
        [model_params(:,:,i_init), ...
            model_fit(:,i_init) ,~,~, C_t_m(:,:,i_init)] =...
                run_madym_lite('DIBEM', C_t_n(:,1:n_t),... 
                'input_Ct', 1, ... 
                'M0_ratio', 1, ... 
                'dyn_times', t,...
                'hct', args.hct, ...
                'dose', args.dose, ...
                'aifName', aif_name,...
                'first_image', 0, ... 
                'injection_image', injection_img, ...
                'init_params', init_params{i_init},...
                'fixed_params', fixed_params{i_init},...
                'dummy_run', 0 ...
                );
    end

    save(...
        sprintf('%smodel_fit_free.mat', results_dir),...
        'model_params', 'model_fit', 'C_t_m');
    
    %Convert model parameters in to physiologically meaningful forms
    fc_data = convert_to_phys(model_params);

    save(sprintf('%sfc_data.mat', results_dir),...
        'fc_data');
    
end
%%-------------------------------------------------------------------------
if args.do_analysis
    %
    load(sprintf('%smodel_fit_free.mat', results_dir),...
       'model_fit');
    load(sprintf('%sfc_data.mat', results_dir),...
        'fc_data');
    load(sprintf('%sC_t.mat', results_dir),...
        'C_t');
    
    valid_pts = all(model_fit, 2);
    
    n_t = size(C_t, 2);
    aic_scores = inf(n_pts2, 3);
    model_k = [5 6 7];        
    for i_m = 1:3
        aic_scores(:,i_m) = ...
            compute_AIC(model_fit(:,i_m), n_t, model_k(i_m));
    end
    [best_aic, aic_model] = min(aic_scores, [], 2); %#ok
    [best_sse, sse_model] = min(model_fit, [], 2); %#ok
    
    aic_model(~valid_pts) = NaN;
    sse_model(~valid_pts) = NaN;
    
    selection_data.aic.active.model = aic_model(1:n_pts);
    selection_data.aic.cxm.model = aic_model(n_pts+1:end);
    selection_data.sse.active.model = sse_model(1:n_pts);
    selection_data.sse.cxm.model = sse_model(n_pts+1:end);
    
    %Set up param containers
    for i_reg = 1:2
        regime = regimes{i_reg};
        for i_param = 1:6
            param = param_data.(regime).params{i_param};
            selection_data.aic.(regime).(param) = NaN(n_pts,1);
            selection_data.sse.(regime).(param) = NaN(n_pts,1);
        end
    end
    for i_param = 1:4
        param = param_data.cxm.params{i_param};
        selection_data.aic.active.cxm.(param) = NaN(n_pts,1);
        selection_data.sse.active.cxm.(param) = NaN(n_pts,1);
    end
    
    %Now swap voxels where 2nd and 3rd models preferred
    for i_m = 1:3           
        for i_reg = 1:2
            regime = regimes{i_reg};
            aic_idx = selection_data.aic.(regime).model == i_m;
            sse_idx = selection_data.sse.(regime).model == i_m;
            for i_param = 1:6
                param = param_data.(regime).params{i_param};
                selection_data.aic.(regime).(param)(aic_idx) = ...
                    fc_data.(regime).(param)(aic_idx,i_m);
                selection_data.sse.(regime).(param)(sse_idx) = ...
                    fc_data.(regime).(param)(sse_idx,i_m);
            end
        end
        for i_param = 1:4
            param = param_data.cxm.params{i_param};
            selection_data.aic.active.cxm.(param)(aic_idx) = ...
                fc_data.active.cxm.(param)(aic_idx,i_m);
            selection_data.sse.active.cxm.(param)(sse_idx) = ...
                fc_data.active.cxm.(param)(sse_idx,i_m);
        end
    end   
    
    selection_data.aic.active.active_mask = make_active_mask(...
        selection_data.aic.active.cxm, args.hct);
    selection_data.sse.active.active_mask = make_active_mask(...
        selection_data.sse.active.cxm, args.hct);
    selection_data.aic.cxm.active_mask = make_active_mask(...
        selection_data.aic.cxm, args.hct);
    selection_data.sse.cxm.active_mask = make_active_mask(...
        selection_data.sse.cxm, args.hct);
    
    %Save the table data
    save(sprintf('%sselection_data.mat', results_dir),...
        'selection_data');
end
%% ------------------------------------------------------------------------
if args.do_plotting
    
    load(sprintf('%sfc_data.mat', results_dir),...
        'fc_data');
    load(sprintf('%sC_t.mat', results_dir),...
        'C_t_n');
    
    load(sprintf('%sselection_data.mat', results_dir),...
        'selection_data');
    %----------------------------------------------------------------------
    display_params.active = {'F_p', 'v_{ecs}', 'k_i', 'k_{ef}', 'f_a', '\tau_a'};
    display_params.cxm = {'F_p', 'PS', 'v_e', 'v_p', 'f_a', '\tau_a'};
    display_name.active = 'Active:';
    display_name.cxm = 'Exchange:';
    
    %1) Average time series
    figure('Name', 'Average C(t)');
    subplot(1,2,1);
    plot(nanmean(C_t_n(1:n_pts,:)));
    title('Liver - active');

    subplot(1,2,2);
    plot(nanmean(C_t_n(n_pts+1:end,:)));
    title('Tumour - 2CXM');
    if ~isempty(args.fig_dir)
        saveas(gcf, sprintf('%s/C_t.png',...
            args.fig_dir));
    end
            
    %2) Histograms of selections
    figure('Name', 'Model selections');
    subplot(1,2,1);
    hist([selection_data.aic.active.model, selection_data.aic.cxm.model],...
        1:3); %#ok
    title('Model selection - AIC');
    legend({'Active', 'CXM'});
    subplot(1,2,2);
    hist([selection_data.sse.active.model, selection_data.sse.cxm.model],...
        1:3); %#ok
    title('Model selection - SSE');
    legend({'Active', 'CXM'});
    if ~isempty(args.fig_dir)
        saveas(gcf, sprintf('%s/model_selection.png',...
            args.fig_dir));
    end
    
    %Boxplots of errors
    boxplot_lims.active =   [1.5 0.5 0.25 0.25 1.2 0.4];
    boxplot_lims.cxm =      [1.5 0.1 0.25 0.25 1.2 0.25];
    for i_reg = 1:2
        regime = regimes{i_reg};
        regime_d = display_name.(regime);
        figure;
        for i_param = 1:6
            param = param_data.(regime).params{i_param};
            param_d = display_params.(regime){i_param};
            
            subplot(2,3,i_param);
            boxplot(...
                [fc_data.(regime).(param) ...
                selection_data.aic.(regime).(param) ...
                selection_data.sse.(regime).(param)]...
                 - mc_data.(regime).(param)(:), 'notch', 'on');
             hold all;
             
             plot(xlim, [0 0], 'k--');
             ylim(boxplot_lims.(regime)(i_param)*[-1 1]);
             xticklabels({'I2', 'I3', 'I4', 'AIC', 'SSE'});
             title(sprintf('%s {\\it %s}', regime_d, param_d));
             if i_param == 1 || i_param == 4
                ylabel('Error (predicted - true)');
             end
             set(gca, 'FontSize', 14);
        end
        if ~isempty(args.fig_dir)
            saveas(gcf, sprintf('%s/param_err_boxplot_%s.png',...
                args.fig_dir, regime));
        end
    end

    % Show parameter fits for:
    % - models fitted at the correct f_a
    % - models fitted at different starting points
    % - models following local fit
    error_stats = cell(5,10);
    table_cols = cell(1,10);
    curr_col = 0;

    %Each IRF model
    for i_m = 1:3
        for i_reg = 1:2
            regime = regimes{i_reg};
            
            curr_col = curr_col +1;
            table_cols{curr_col} = ...
                ['IRF ' num2str(i_m + 1) ' - ' regime];
            figure('Name', table_cols{curr_col});
            for i_param = 1:6

                subplot(2,3,i_param);
                param = param_data.(regime).params{i_param};
                param_d = display_params.(regime){i_param};
                
                [error_stats{i_param, curr_col}] =...
                plot_param(...
                    mc_data.(regime).(param),...
                    fc_data.(regime).(param)(:,i_m), param_d,...
                    i_param == 1 || i_param == 4);
            end
            if ~isempty(args.fig_dir)
                saveas(gcf, sprintf('%s/param_corr_%s_IRF%d.png',...
                    args.fig_dir, regime, i_m+1));
            end
        end        
    end
    
    selections = {'aic', 'sse'};
    for i_sel = 1:2
        selection = selections{i_sel};
        for i_reg = 1:2
            regime = regimes{i_reg};
            
            curr_col = curr_col +1;
            table_cols{curr_col} = ...
                [selection ' - ' regime];
            figure('Name', table_cols{curr_col});
            for i_param = 1:6

                subplot(2,3,i_param);
                param = param_data.(regime).params{i_param};
                param_d = display_params.(regime){i_param};
                
                [error_stats{i_param, curr_col}] =...
                plot_param(...
                    mc_data.(regime).(param),...
                    selection_data.(selection).(regime).(param), param_d,...
                    i_param == 1 || i_param == 4);
            end
            if ~isempty(args.fig_dir)
                saveas(gcf, sprintf('%s/param_corr_%s_%s.png',...
                    args.fig_dir, regime, selection));
            end
        end       
    end
 
    %Save the table data
    save(sprintf('%stable_data.mat', results_dir),...
        'error_stats', 'table_cols');
end
%%-------------------------------------------------------------------------
function [fc_data] = convert_to_phys(model_params)
    %
    fc_data = [];
    [...
        fc_data.active.F_p, fc_data.active.v_ecs, ...
        fc_data.active.k_i, fc_data.active.k_ef] = ...
        active_params_model_to_phys(...
            squeeze(model_params(1:n_pts,1,:)),...
            squeeze(model_params(1:n_pts,2,:)),...
            squeeze(model_params(1:n_pts,3,:)),...
            squeeze(model_params(1:n_pts,4,:))...
         );
    [...
        fc_data.active.cxm.F_p, fc_data.active.cxm.PS, ...
        fc_data.active.cxm.v_e, fc_data.active.cxm.v_p] = ...
        two_cx_params_model_to_phys(...
            squeeze(model_params(1:n_pts,1,:)),...
            squeeze(model_params(1:n_pts,2,:)),...
            squeeze(model_params(1:n_pts,3,:)),...
            squeeze(model_params(1:n_pts,4,:))...
         );
    fc_data.active.f_a = squeeze(model_params(1:n_pts,5,:));
    fc_data.active.tau_a = squeeze(model_params(1:n_pts,6,:));

    %Cxm forms
    [...
       fc_data.cxm.F_p, fc_data.cxm.PS, ...
       fc_data.cxm.v_e, fc_data.cxm.v_p] = ...
        two_cx_params_model_to_phys(...
            squeeze(model_params((n_pts+1):end,1,:)),...
            squeeze(model_params((n_pts+1):end,2,:)),...
            squeeze(model_params((n_pts+1):end,3,:)),...
            squeeze(model_params((n_pts+1):end,4,:))...
        );
    fc_data.cxm.f_a = squeeze(model_params((n_pts+1):end,5,:));
    fc_data.cxm.tau_a = squeeze(model_params((n_pts+1):end,6,:));
    
    fc_data.active.active_mask = make_active_mask(...
        fc_data.active.cxm, args.hct);
    fc_data.cxm.active_mask = make_active_mask(...
        fc_data.cxm, args.hct);
end
%% ------------------------------------------------------------------------
function [stats] = plot_param(param_gt, param_pred, param, show_ylabel)
    param_gt = param_gt(:);
    param_pred = param_pred(:);
    err = param_gt - param_pred;
    min_param = min(param_gt);
    max_param = max(param_gt);
    
    plot(param_gt, err, 'r.');
    hold all;
    
    plot([min_param max_param], [0 0], 'k--');
    axis([min_param max_param -max_param/2 max_param/2]);     
    
    xlabel(['{\it ' param '} (true)']);
    
    if show_ylabel
        ylabel('Error (predicted - true)');
    end
    
    valid = isfinite(param_gt) & isfinite(param_pred); 
    mae = nanmedian(abs(err));
    
    if nnz(valid) > 4
        ad_h = adtest(err(valid));
    else
        ad_h = NaN;
    end
    
    err95_idx = (abs(err) < prctile(abs(err), 95)) & valid;
    if any(err95_idx)
        rho = corr(param_gt(err95_idx), param_pred(err95_idx));
        linfit = polyfit(param_gt(err95_idx), param_pred(err95_idx), 1);
    else
        rho = NaN;
        linfit = [NaN NaN];
    end
    
    stats.err = err;
    stats.mae = mae;
    stats.err_iqr = iqr(abs(err));
    stats.median = nanmedian(param_pred);
    stats.median_gt = nanmedian(param_gt);
    stats.adtest = ad_h;
    stats.pct_95 = prctile(err, [2.5 97.5]);
    stats.n_discarded = sum(~valid);
    stats.rho = rho;
    stats.linfit = linfit;
    
    title(...
        sprintf('MAE = %3.2f, %d invalid samples', mae, nnz(~valid)));
    set(gca, 'FontSize', 14);
    %title({sprintf('r = %3.2f', rho); ...
    %    sprintf('MAE = %3.2f, n = %d, AD = %d', mae, nnz(valid), ad_h)});
end
%% ------------------------------------------------------------------------
function [param_sample] = sample_from_param_dist(param, regime, n_pts)
    rand_idx = sample_from_probs(...
            param_data.(regime).(param).dist, n_pts);
        
    param_sample = param_data.(regime).(param).bins(rand_idx);
end
    
%% ------------------------------------------------------------------------
function [bins] = get_bins(param, regime)

    %Go through pre-computed analysis, and estimate 5th and 95th percentile
    %of parameter distribution
    param_pctiles = zeros(11,3);
    for i_pt = 1:11
        param_pctiles(i_pt,:) = ...
            roi_analysis{i_pt}.api4.liver.(regime).y.(param)([2 6 7]);
    end

    pct_5_95_m = median(param_pctiles(:,1:2));
    pct_max = max(param_pctiles(:,3));
    
    %Set a bin width based on range from 5th to 95th percentile
    bin_range = pct_5_95_m(2) - pct_5_95_m(1);
    bin_width = bin_range / 90;
    
    %Extend bins, but just linearly, we won't extend to full margins as
    %don't want to sample points here (most likely outliers)
    bin0 = max(0,pct_5_95_m(1) - 5*bin_width);
    bin100 = min(pct_max, pct_5_95_m(2) + 5*bin_width);
    bins = linspace(bin0,bin100, 101);
end

%% ------------------------------------------------------------------------
function [] = update_param_distribution(param, regime, param_maps, mask)
    if ~nnz(mask)
        return;
    end
    
    %Take histogram count of parameter for this subject
    param_counts = hist(...
        param_maps.(param)(mask), param_data.(regime).(param).bins); %#ok
    
    %Add to the overall distribution
    param_data.(regime).(param).counts = ...
        param_data.(regime).(param).counts + param_counts;
end

%% ------------------------------------------------------------------------
function [] = update_distributions(i_sub)

    %Set up paths to subject's analysis data and ROI masks
    subject_id = ['PRIMDCE_' num2str(i_sub)];
    subject_dir = [args.study_dir '/' subject_id '/visit1/'];
    analysis_dir = [subject_dir args.model_conc_dir '/'];
    mask_dir = [subject_dir 'analysis_masks/'];
    liver_mask = load_img_volume([mask_dir 'liver_mask.hdr']) > 0;
    tumour_mask = load_img_volume([mask_dir 'tumour_mask.hdr']) > 0;
    valid_mask = liver_mask | tumour_mask;

    %Load in model parameter maps
    F_pos = load_img_volume([analysis_dir 'Fpos.hdr']);
    F_neg = load_img_volume([analysis_dir 'Fneg.hdr']);
    K_pos = load_img_volume([analysis_dir 'Kpos.hdr']);
    K_neg = load_img_volume([analysis_dir 'Kneg.hdr']);
    f_a = load_img_volume([analysis_dir 'fa.hdr']);
    tau_a = load_img_volume([analysis_dir 'aoffset.hdr']);

    %Convert to physiological parameters for each regime
    [active_params.F_p, ...
        active_params.v_ecs, active_params.k_i, active_params.k_ef] = ...
        active_params_model_to_phys(...
        F_pos(valid_mask), ...
        F_neg(valid_mask), ...
        K_pos(valid_mask), ...
        K_neg(valid_mask), 0);
    active_params.f_a = f_a(valid_mask);
    active_params.tau_a = tau_a(valid_mask);

    [cxm_params.F_p, cxm_params.PS, cxm_params.v_e, cxm_params.v_p] = ...
        two_cx_params_model_to_phys(...
        F_pos(valid_mask), ...
        F_neg(valid_mask), ...
        K_pos(valid_mask), ...
        K_neg(valid_mask), 0);
    cxm_params.f_a = f_a(valid_mask);
    cxm_params.tau_a = tau_a(valid_mask);

    %Set active mask based on v_e + v_p in each CXM regime
    active_mask = make_active_mask(cxm_params, args.hct);

    %Loop through the parameter maps for each regime, updating the parameter
    %distribution
    for par = param_data.active.params
        update_param_distribution(...
            par{1}, 'active', active_params, active_mask & liver_mask(valid_mask));
    end
    for par = param_data.cxm.params
        update_param_distribution(...
            par{1}, 'cxm', cxm_params, ~active_mask & tumour_mask(valid_mask));
    end

end
%% ***********************************************************************
function noise_time_series = get_noise_time_series()
    noise_time_series = zeros(n_pts2, n_t);
    curr_vox = 0;
    num_subs = length(args.subject_names);
    for i_sub = 1:num_subs
        subject_dir = [args.study_dir '/' args.subject_names{i_sub} ...
            '/visit1/'];

        if i_sub < num_subs
            vox_idx = curr_vox + (1:round(n_pts2/num_subs));
            curr_vox = vox_idx(end);
        else
            vox_idx = (curr_vox+1):n_pts2;
        end
        n_vox = length(vox_idx);

        roi_mask = load_img_volume(...
            [subject_dir args.roi_mask]);

        for i_ero = 1:3
            roi_mask = imerode(...
                roi_mask, strel('disk', 1));
        end
        roi_slices = squeeze(any(any(roi_mask,1),2));
        
        if nnz(roi_slices) > 7
            slice1 = find(roi_slices, 3, 'first');
            slice2 = find(roi_slices, 3, 'last');
            roi_mask(:,:,[slice1 slice2]) = 0;
        end
            
        mask_voxels = find(roi_mask);
        rand_idx = randperm(length(mask_voxels), n_vox);
        subset_mask = false(size(roi_mask));
        subset_mask(mask_voxels(rand_idx)) = 1;

        dyn_conc_dir = [subject_dir args.dyn_conc_dir '\'];
        model_conc_dir = [subject_dir args.model_conc_dir '\'];

        C_ti = get_dyn_vals(...
            [dyn_conc_dir 'Ct_sig'], n_t, subset_mask);
        C_t_mi = get_dyn_vals(...
            [model_conc_dir 'Ct_mod'], n_t, subset_mask);

        noise_time_series(vox_idx,:) = C_ti - C_t_mi;
    end
    noise_time_series = noise_time_series(randperm(n_pts2),:);
end
%% ------------------------------------------------------------------------
end
function active_mask = make_active_mask(model, hct)
    active_mask = (model.v_e + model.v_p/(1 - hct)) > 1.0;
end