function [] = milano_monte_carlo_fixed(varargin)
%MILANO_MONTE_CARLO *Insert a one line summary here*
%   [] = milano_monte_carlo(varargin)
%
% MILANO_MONTE_CARLO uses the U_PACKARGS interface function
% and so arguments to the function may be passed as name-value pairs
% or as a struct with fields with corresponding names. The field names
% are defined as:
%
% Mandatory Arguments:
%
% Optional Arguments:
%
% Outputs:
%
% Example:
%
% Notes:
%
% See also:
%
% Created: 06-Jan-2020
% Author: Michael Berks 
% Email : michael.berks@manchester.ac.uk 
% Phone : +44 (0)161 275 7669 
% Copyright: (C) University of Manchester 

% Unpack the arguments:
args = u_packargs(varargin, 0, ...
    'experiment_root', 'Q:/data/MB/milano_primovist/experiments/monte_carlo',...
    'results_dir', 'fixed',...
    'analysis_dir', 'analysis',...
    'n_pts', 1e3,...
    'n_t', 100,...
    'temporal_resolution', 0,...
    'end_time', 6,...
    'injection_image', 0,...
    'injection_time', 60,...
    'noise_idx', 1:4,...
    'noise_sigma', 10:10:50,...
    'gt_idx', 1:4,...
    'do_fitting', 0,...
    'use_real_aif', 0,...
    'use_real_noise', 0,...
    'study_dir', [],...
    'subject_list', [],...
    'dyn_conc_dir', [],...
    'model_conc_dir', [],...
    'aif_name', [],...
    'visits', [],...
    'roi_mask', [],...
    'use_M0', 0,...
    'use_existing_data', 1,...
    'use_models', 1:3,...
    'do_analysis', 0,...
    'do_plotting', 0,...
    'irf2_2cxm', 0,...
    'dummy_run', 0);
clear varargin;

% Experiments to run
% Prelude: Determine noise levels that give appropriate range - lowest value
% should be slightly lower than motion-corrected data, highest value should
% should be slightly higher than original data.
%
% 1) With fixed parameters for each model type, run experiment with
% different levels of added rician noise
%
% 2) Using the estimated values from the real data, generate a population
% of samples, using noise time-series from the real data
%
% Experimental form:
% 1) Generate 1e4 samples for each model form (what AIF/PIF to use?)
%
% 2) Use Madym lite to fit the 6 functional forms
%
% 3) Choose the appropriate model form for each voxel - compare to GT
%
% 4) Choose the appropriate interpretation for each voxel - compare to GT
%
% 5) Compare parameters to GT
%   - for selected model
%   - correct model
%%
%Set up times and IFs
n_pts = args.n_pts;
dose = 0.025;

if args.use_real_aif
    aif_name = [args.study_dir '/' args.aif_name ];
    aif = load_aif(aif_name, 0.42);
    t = aif(1:args.n_t,1);
    Ca_t = aif(1:args.n_t,2);
    n_t = length(t);
    args.end_time = t(end);
    aif_str = '_real_aif';
else
    n_t = ceil(args.end_time / (args.temporal_resolution/60)) + 1;
    t = linspace(0,args.end_time,n_t);
    aif_name = [];
    aif_str = [];
end
temporal_resolution = 60*t(2)-t(1);

%Get injection image from injection time if not explicitly set
if args.injection_image
    injection_image = args.injection_image;
else
    [~,injection_image] = min(abs(t - args.injection_time/60));
end

%Now we've sorted times and injection image, if we're using population AIF,
%we can create it
if ~args.use_real_aif
    Ca_t = population_aif(t, injection_image, 'dose', dose);
end

%For both real and pop AIF, compute derived PIF
Cv_t = compute_PIF(Ca_t, [], t);

%Creat results dir from input info
results_dir = sprintf('%s/%s/tr%2.1f_et%2.1f%s/',...
    args.experiment_root, args.results_dir,...
    temporal_resolution, args.end_time, aif_str);

fprintf('n_t = %d, temp res = %3.2f, injection image = %d\n',...
        n_t, temporal_resolution, injection_image);
fprintf('Saving results to %s\n', results_dir);
if args.dummy_run
    return;
end
create_folder(results_dir);

%%
% Model forms
% i)
%%
%A1: Active + efflux
%A2: Active, no efflux
F_p_active = 0.5;
f_a_active = 0.2;
tau_a_active = 0.1;
v_ecs_active = 0.15;
k_i_active = 0.07;
k_ef_active = 0.05;
%A3: Active, no uptake
F_p_active_lo = 0.15;
v_ecs_active_hi = 0.5;
%%
%X1: 2CXM
F_p_cxm = 0.4;
f_a_cxm = 0.6;
tau_a_cxm = 0.05;
PS_cxm = 0.05;
v_e_cxm = 0.12;
v_p_cxm = 0.10;
%%
%F_pos, F_neg, K_pos, K_neg, fa, tau_a, tau_v
fixed_params{1} = [1 3 7];  % i22
fixed_params{2} = [3 7];    % i23
fixed_params{3} = [7]; %#ok % i24

init_params{1} = [0.0,0.2,0.0,4.0,0.25,0.025,0.0];
init_params{2} = [0.2,0.2,0.0,4.0,0.25,0.025,0.0];
init_params{3} = [0.2,0.2,0.5,4.0,0.25,0.025,0.0];
            
%%
F_pos = zeros(1,4);
F_neg = zeros(1,4);
K_pos = zeros(1,4);
K_neg = zeros(1,4);
f_a = [f_a_active f_a_active f_a_active f_a_cxm];
tau_a = [tau_a_active tau_a_active tau_a_active tau_a_cxm];

%Active forms
[F_pos(1), F_neg(1), K_pos(1), K_neg(1)] = ...
    active_params_phys_to_model(...
    F_p_active, v_ecs_active, k_i_active, k_ef_active);
[F_pos(2), F_neg(2), K_pos(2), K_neg(2)] = ...
    active_params_phys_to_model(...
    F_p_active, v_ecs_active, k_i_active, 0);
[F_pos(3), F_neg(3), K_pos(3), K_neg(3)] = ...
    active_params_phys_to_model(...
    F_p_active_lo, v_ecs_active_hi, 0, 0);

%Cxm forms
[F_pos(4), F_neg(4), K_pos(4), K_neg(4)] = ...
    two_cx_params_phys_to_model(...
    F_p_cxm, PS_cxm, v_e_cxm, v_p_cxm, false);
%%
[C_t] = diirf_model(...
    F_pos, F_neg, K_pos, K_neg, ... 
    f_a, tau_a, Ca_t, Cv_t, t, 0);

FA = 20;
TR = 2.4;
relax_coeff = 14.0;
T1_0 = 1000;
M0 = 100;

% display_time_series(C_t, 'plot_rows', 2, 'plot_cols', 3);
% display_time_series(S_t, 'plot_rows', 2, 'plot_cols', 3);
%%
if args.do_fitting
%Loop through noise
    for i_noise = args.noise_idx
        sigma = args.noise_sigma(i_noise);

        %Loop through model forms
        for i_gt = args.gt_idx
            fprintf('Fitting model %d, noise %d\n', i_gt, i_noise);
            
            if args.use_existing_data && exist(...
                    sprintf('%smodel_fit_n%d_m%d.mat',...
                    results_dir, i_noise, i_gt), 'file')

                load(...
                    sprintf('%smodel_fit_n%d_m%d.mat',...
                    results_dir, i_noise, i_gt),...
                    'model_params', 'model_fit', 'C_t_m', 'C_t_n');

            else
                if args.use_real_noise
                    noise = get_noise_time_series();
                    C_t_n = repmat(C_t(i_gt,:), n_pts, 1) + noise;
                else
                    [S_t] = ...
                        concentration_to_signal(...
                        C_t, FA, TR, T1_0, M0, relax_coeff,...
                        injection_image-1);

                    %Make noise copies of signal
                    S_t_n = add_rician_noise(...
                        repmat(S_t(i_gt,:), n_pts, 1), sigma);

                    %Convert back to concentration
                    if args.use_M0
                        [C_t_n] = ...
                            signal_to_concentration(...
                            S_t_n, FA, TR, T1_0, relax_coeff,...
                            injection_image-1, M0);
                    else
                        [C_t_n] = ...
                            signal_to_concentration(...
                            S_t_n, FA, TR, T1_0, relax_coeff,...
                            injection_image-1);
                    end
                end
                
                model_params = zeros(n_pts, 7, 3);
                model_fit = zeros(n_pts, 3);
                C_t_m = zeros(n_pts, n_t, 3);
            end

            %Fit each model in frame work
            for i_m = args.use_models
                [model_params(:,:,i_m), model_fit(:,i_m) ,~,~, C_t_m(:,:,i_m)] =...
                    run_madym_lite('DIBEM', C_t_n,... 
                    'input_Ct', 1, ... 
                    'M0_ratio', 1, ... 
                    'dyn_times', t,...
                    'hct', 0.42, ...
                    'dose', dose, ...
                    'aifName', aif_name,...
                    'first_image', 0, ... 
                    'injection_image', injection_image, ...
                    'init_params', init_params{i_m},... 
                    'fixed_params', fixed_params{i_m},...
                    'dummy_run', 0 ...
                    );
            end
            save(...
                sprintf('%smodel_fit_n%d_m%d.mat', results_dir, i_noise, i_gt),...
                'model_params', 'model_fit', 'C_t_m', 'C_t_n');

        end
    end
end
%%
if args.do_analysis
    %Load in the model data
    gt_models = [3 2 1 3];
    param_vals = {
        [F_p_active f_a_active tau_a_active v_ecs_active k_i_active k_ef_active]
        [F_p_active f_a_active tau_a_active v_ecs_active k_i_active 0]
        [F_p_active_lo f_a_active tau_a_active v_ecs_active_hi 0 0]
        [F_p_cxm f_a_cxm tau_a_cxm PS_cxm v_e_cxm v_p_cxm]
    };

    if args.irf2_2cxm
        param_names = {
            {'F_p' 'f_a' 'tau_a' 'v_ecs' 'k_i' 'k_ef'}
            {'F_p' 'f_a' 'tau_a' 'v_ecs' 'k_i' 'k_ef'}
            {'F_p' 'f_a' 'tau_a' 'v_e' 'PS' 'v_p'}
            {'F_p' 'f_a' 'tau_a' 'PS' 'v_e' 'v_p'}
        };
    else
        param_names = {
            {'F_p' 'f_a' 'tau_a' 'v_ecs' 'k_i' 'k_ef'}
            {'F_p' 'f_a' 'tau_a' 'v_ecs' 'k_i' 'k_ef'}
            {'F_p' 'f_a' 'tau_a' 'v_ecs' 'k_i' 'k_ef'}
            {'F_p' 'f_a' 'tau_a' 'PS' 'v_e' 'v_p'}
        };
    end
    param_lims = {
        [2.0 1.0 0.5 1.0 0.5 0.5]
        [2.0 1.0 0.5 1.0 0.5 0.5]
        [2.0 1.0 0.5 1.0 0.5 0.5]
        [2.0 1.0 0.5 1.0 0.25 1.0]
    };
    
    analysis_dir = [results_dir args.analysis_dir '/'];
    create_folder(analysis_dir);
    
    fid_aic = fopen([analysis_dir 'all_model_forms_aic.txt'], 'wt');
    fid_sse = fopen([analysis_dir 'all_model_forms_sse.txt'], 'wt');
    for i_gt = args.gt_idx
        fprintf(fid_aic,...
            'Model selection analysis for model form %d\n', i_gt);
        fprintf(fid_sse,...
            'Model selection analysis for model form %d\n', i_gt);
        fprintf(1,...
            'Model selection analysis for model form %d\n', i_gt);
        for i_noise = args.noise_idx

            load(...
                sprintf('%smodel_fit_n%d_m%d.mat', results_dir, i_noise, i_gt),...
                'model_params', 'model_fit', 'C_t_m', 'C_t_n');

            pred = [];
            aic_model = [];
            sse_model = [];
            process_params(...
                param_vals{i_gt}, param_names{i_gt}, param_lims{i_gt},...
                gt_models(i_gt), i_gt <= 3);
        end
    end
    fclose(fid_aic);
    fclose(fid_sse);
end
%% ***********************************************************************
    function process_params(param_vals, param_names, param_lims, ...
            gt_model, gt_active)
        
    
    aic_scores = inf(n_pts, 3);
    aic_model_idx = false(n_pts,3);
    sse_model_idx = false(n_pts,3);

    model_k = [5 6 7];
    dibem_model = 3;
    irf_models = {1, 2, 3};
    
    model_fit(:,setdiff(1:size(model_fit,2), args.use_models)) = inf;
        
    for i_mod = args.use_models
        aic_scores(:,i_mod) = ...
            compute_AIC(model_fit(:,i_mod), n_t, model_k(i_mod));
    end
    [best_aic, aic_model] = min(aic_scores, [], 2); %#ok
    [best_sse, sse_model] = min(model_fit, [], 2); %#ok

    pred.f_a = squeeze(model_params(:,5,:));
    pred.tau_a = squeeze(model_params(:,6,:));

    [pred.active.F_p, pred.active.v_ecs, pred.active.k_i, pred.active.k_ef] =...
        active_params_model_to_phys(...
        squeeze(model_params(:,1,:)),...
        squeeze(model_params(:,2,:)),...
        squeeze(model_params(:,3,:)),...
        squeeze(model_params(:,4,:)));

    [pred.cxm.F_p, pred.cxm.PS, pred.cxm.v_e, pred.cxm.v_p] =...
        two_cx_params_model_to_phys(...
        squeeze(model_params(:,1,:)),...
        squeeze(model_params(:,2,:)),...
        squeeze(model_params(:,3,:)),...
        squeeze(model_params(:,4,:)));
    %
    pred.opt.F_p = pred.active.F_p(:,dibem_model);
    pred.opt.f_a = pred.f_a(:,dibem_model);
    pred.opt.tau_a = pred.tau_a(:,dibem_model);

    pred.opt.v_ecs = pred.active.v_ecs(:,dibem_model);
    pred.opt.k_i = pred.active.k_i(:,dibem_model);
    pred.opt.k_ef = pred.active.k_ef(:,dibem_model);

    pred.opt.PS = pred.cxm.PS(:,dibem_model);
    pred.opt.v_e = pred.cxm.v_e(:,dibem_model);
    pred.opt.v_p = pred.cxm.v_p(:,dibem_model);
    pred.aic = pred.opt;
    %
    t_offset = t - t(injection_image);
    t_1_min = (t_offset > 1) & (t_offset < 2);
    t_4_min = (t_offset > 4) & (t_offset < 5);
    post_1_min = mean(C_t_m(:,t_1_min,:),2);
    post_4_min = mean(C_t_m(:,t_4_min,:),2);
    accum_idx = post_4_min > post_1_min;
    accumulation_aic = accum_idx(:, dibem_model);
    accumulation_opt = accum_idx(:, dibem_model);
    
    post_1_min = mean(C_t_n(:,t_1_min),2);
    post_4_min = mean(C_t_n(:,t_4_min),2);
    accumulation_signal = post_4_min > post_1_min;
    
    %
    for i_mod = setdiff(args.use_models, dibem_model)
        sse_idx = sse_model == i_mod;
        pred.opt.F_p(sse_idx) = pred.active.F_p(sse_idx,i_mod);
        pred.opt.f_a(sse_idx) = pred.f_a(sse_idx,i_mod);
        pred.opt.tau_a(sse_idx) = pred.tau_a(sse_idx,i_mod);

        pred.opt.v_ecs(sse_idx) = pred.active.v_ecs(sse_idx,i_mod);
        pred.opt.k_i(sse_idx) = pred.active.k_i(sse_idx,i_mod);
        pred.opt.k_ef(sse_idx) = pred.active.k_ef(sse_idx,i_mod);

        pred.opt.PS(sse_idx) = pred.cxm.PS(sse_idx,i_mod);
        pred.opt.v_e(sse_idx) = pred.cxm.v_e(sse_idx,i_mod);
        pred.opt.v_p(sse_idx) = pred.cxm.v_p(sse_idx,i_mod);
        accumulation_opt(sse_idx) = accum_idx(sse_idx,i_mod);
        
        aic_idx = aic_model == i_mod;
        pred.aic.F_p(aic_idx) = pred.active.F_p(aic_idx,i_mod);
        pred.aic.f_a(aic_idx) = pred.f_a(aic_idx,i_mod);
        pred.aic.tau_a(aic_idx) = pred.tau_a(aic_idx,i_mod);

        pred.aic.v_ecs(aic_idx) = pred.active.v_ecs(aic_idx,i_mod);
        pred.aic.k_i(aic_idx) = pred.active.k_i(aic_idx,i_mod);
        pred.aic.k_ef(aic_idx) = pred.active.k_ef(aic_idx,i_mod);

        pred.aic.PS(aic_idx) = pred.cxm.PS(aic_idx,i_mod);
        pred.aic.v_e(aic_idx) = pred.cxm.v_e(aic_idx,i_mod);
        pred.aic.v_p(aic_idx) = pred.cxm.v_p(aic_idx,i_mod);
        accumulation_aic(aic_idx) = accum_idx(aic_idx,i_mod);
    end

    for i_mod = args.use_models
        aic_model_idx(:,i_mod) = aic_model==i_mod;
        sse_model_idx(:,i_mod) = sse_model==i_mod;
    end
    
    irf_idx = false(n_pts,3);
    for irf = 1:3
        irf_idx(:,irf) = ismember(aic_model, irf_models{irf});
    end
    [~,selected_irf] = max(irf_idx,[],2);
    active_idx = (...
        pred.cxm.v_e(:,dibem_model) + ...
        pred.cxm.v_p(:,dibem_model)/(1-0.42)) > 1;

    
    %Make indicator map of variables
    indicator_map = make_DIBEM_indicator_map(...
        pred.aic, pred.aic, selected_irf, ...
        accumulation_aic, accumulation_signal);
    tse = compute_time_series_error(C_t_n, 18);
    display_DIBEM_factor_counts(...
        indicator_map, true(size(indicator_map)), ...
        tse);
    display_DIBEM_factor_counts(...
        indicator_map, true(size(indicator_map)), ...
        tse,...
        sprintf('%smodel_fit_factor_n%d_m%d.txt',...
            analysis_dir, i_noise, i_gt));
    
    fprintf('\n***************************************************\n');
    fprintf('Accumulation, signal = %2.1f, AIC = %2.1f, agreement = %2.1f\n',...
        100*sum(accumulation_signal)/n_pts, ...
        100*sum(accumulation_aic)/n_pts,...
        100*sum(accumulation_signal == accumulation_aic)/n_pts);
    fprintf('\n***************************************************\n');

    %How often is the correct model selected?
    gt_irf = ceil(gt_model/2);
    
    correct_model = sum(aic_model == gt_model) / 10;
    correct_irf = sum(irf_idx(:,gt_irf)) / 10;
    
    if gt_active
        correct_active = sum(active_idx)/10;
    else
        correct_active = 100 - sum(active_idx)/10;
    end
    
    % Log model selection results in local file
    fid = fopen(sprintf('%smodel_fit_results_n%d_m%d.txt',...
        analysis_dir, i_noise, i_gt),'wt');
    
    fprintf(fid,...
        'Model selection analysis for model form %d, noise = %4.3f\n',...
        i_gt, args.noise_sigma(i_noise));
    
    fprintf(fid,...
        'Model selections: %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f\n',...
        sum(aic_model_idx) / 10);
    fprintf(fid,...
        'IRF selections: %4.3f %4.3f %4.3f\n',...
        sum(irf_idx) / 10);
    fprintf(fid,...
        'Active selections: %4.3f %4.3f\n\n',...
        sum(active_idx) / 10, sum(~active_idx) / 10);
    
    fprintf(fid,'Correct model: %3.2f%% \n', correct_model);
    fprintf(fid,'Correct IRF: %3.2f%% \n', correct_irf);
    fprintf(fid,'Correct regime: %3.2f%% \n', correct_active);
    fclose(fid);
    
    % Also log entry in main results file
    fprintf(fid_aic,...
        '%4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f\n',...
        sum(aic_model_idx) / 10);
    fprintf(fid_sse,...
        '%4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f %4.3f\n',...
        sum(sse_model_idx) / 10);
    
    % Compute parameter error analysis
    param_analysis = cell(11, length(param_vals));
    for i_p = 1:length(param_vals)
        for i_mod = args.use_models
            param_analysis{i_mod,i_p} = analyse_model_params(...
                param_names{i_p}, param_vals(i_p), i_mod,...
                aic_model_idx, sse_model_idx, active_idx, accum_idx);
        end
        param_analysis{10,i_p} = analyse_opt_params(...
            param_names{i_p}, param_vals(i_p), 'aic');
        param_analysis{11,i_p} = analyse_opt_params(...
            param_names{i_p}, param_vals(i_p), 'opt');
    end
            
    save(sprintf('%smodel_fit_results_n%d_m%d.mat',...
        analysis_dir, i_noise, i_gt),...
        'aic_model_idx','sse_model_idx','irf_idx',...
        'active_idx','accum_idx','param_analysis',...
        'accumulation_signal','accumulation_aic','accumulation_opt');
    
    if args.do_plotting
        for i_p = 1:length(param_vals)
%             plot_param_hists(...
%                 param_names{i_p}, param_vals(i_p), param_lims(i_p), gt_model);
            plot_param_hists2(...
                param_names{i_p}, param_vals(i_p), param_lims(i_p), ...
                aic_model_idx, sse_model_idx);
        end
    end
    
    end
%% ***********************************************************************
    function [analysis] = analyse_opt_params(param, param_gt, opt_type)
        % Parameter prediction (median, IQR, MAE) when
        % - all voxels
        % - lowest AIC voxels
        % - NOT lowest AIC voxels
        % - lowest SSE voxels
        % - NOT lowest SSE voxels
        % - active voxels
        % - exchange voxels
        % - accumlating voxels
        % - NOT accumlating voxels
        
        param_pred = pred.(opt_type).(param);
        analysis.all = analysis_computer(param_pred, param_gt);
    end
%% ***********************************************************************
    function [analysis] = analyse_model_params(param, param_gt, model,...
            aic_model_idx, sse_model_idx, active_idx, accum_idx)
        % Parameter prediction (median, IQR, MAE) when
        % - all voxels
        % - lowest AIC voxels
        % - NOT lowest AIC voxels
        % - lowest SSE voxels
        % - NOT lowest SSE voxels
        % - active voxels
        % - exchange voxels
        % - accumlating voxels
        % - NOT accumlating voxels
        
        if isfield(pred, param)
            param_pred = pred.(param)(:,model);
        elseif isfield(pred.active, param)
            param_pred = pred.active.(param)(:,model);
        elseif isfield(pred.cxm, param)
            param_pred = pred.cxm.(param)(:,model);
        end
        
        %all voxels
        analysis.all = analysis_computer(param_pred, param_gt);
        
        %lowest AIC
        idx = aic_model_idx(:,model);
        analysis.aic = analysis_computer(param_pred(idx), param_gt);
        
        %NOT lowest AIC
        idx = ~aic_model_idx(:,model);
        analysis.aic_not = analysis_computer(param_pred(idx), param_gt);
        
        %lowest SSE
        idx = sse_model_idx(:,model);
        analysis.sse = analysis_computer(param_pred(idx), param_gt);
        
        %NOT lowest SSE
        idx = ~sse_model_idx(:,model);
        analysis.sse_not = analysis_computer(param_pred(idx), param_gt);
        
        % - active voxels
        analysis.active = analysis_computer(param_pred(active_idx), param_gt);
        
        % - exchange voxels
        analysis.active_not = analysis_computer(param_pred(~active_idx), param_gt);
        
        % - accumlating voxels
        idx = accum_idx(:,model);
        analysis.accum = analysis_computer(param_pred(idx), param_gt);
        
        % - NOT accumlating voxels
        idx = ~accum_idx(:,model);
        analysis.accum_not = analysis_computer(param_pred(idx), param_gt);
        
    end
%% ***********************************************************************
    function [analysis] = analysis_computer(param_vals, param_gt)
        analysis.median = nanmedian(param_vals);
        analysis.iqr = iqr(param_vals);
        analysis.q25_75 = prctile(param_vals, [25 75]);
        analysis.mae = nanmedian(abs(param_vals - param_gt));
    end
%% ***********************************************************************
    function plot_param_hists(param, param_gt, param_lim, gt_model) %#ok
    
    valid_aic = isfinite(pred.aic.(param)) & ~isnan(pred.aic.(param));
    valid_opt = isfinite(pred.opt.(param)) & ~isnan(pred.opt.(param));
    
    param_range = [0 param_lim];%[...
        %min(pred.aic.(param)(valid_aic)), max(pred.aic.(param)(valid_aic))];
    param_bins = linspace(param_range(1), param_range(2), 50);

    param_counts_aic = hist(pred.aic.(param)(valid_aic), param_bins); %#ok
    param_counts_opt = hist(pred.opt.(param)(valid_opt), param_bins); %#ok
    
    if isfield(pred, param)
        valid_gt = ...
            isfinite(pred.(param)(:,gt_model)) & ...
            ~isnan(pred.(param)(:,gt_model));
        param_counts_gt = hist(pred.(param)(valid_gt, gt_model), param_bins); %#ok
        param_median_gt = median(pred.(param)(valid_gt, gt_model));
    elseif isfield(pred.active, param)
        valid_gt = ...
            isfinite(pred.active.(param)(:,gt_model)) &...
            ~isnan(pred.active.(param)(:,gt_model));
        param_counts_gt = hist(pred.active.(param)(valid_gt, gt_model), param_bins); %#ok
        param_median_gt = median(pred.active.(param)(valid_gt, gt_model));
    elseif isfield(pred.cxm, param)
        valid_gt = ...
            isfinite(pred.cxm.(param)(:,gt_model)) & ...
            ~isnan(pred.cxm.(param)(:,gt_model));
        param_counts_gt = hist(pred.cxm.(param)(valid_gt, gt_model), param_bins); %#ok
        param_median_gt = median(pred.cxm.(param)(valid_gt, gt_model));
    end
    
    param_median_aic = median(pred.aic.(param)(valid_aic));
    param_median_opt = median(pred.opt.(param)(valid_opt));

    bar_width = param_bins(2)-param_bins(1);
    %
    figure;
    hold all;
    bar(param_bins, param_counts_aic, 1/3, 'r', ...
        'EdgeColor', 'k');
    bar(param_bins+bar_width/3, param_counts_opt, 1/3, 'g', ...
        'EdgeColor', 'k');
    bar(param_bins+2*bar_width/3, param_counts_gt, 1/3, 'b', ...
        'EdgeColor', 'k');
    title(sprintf('Predictions of %s for model form %d, noise = %4.3f\n',...
        param, i_gt, args.noise_sigma(i_noise)));
    %
    y_lim = get(gca, 'ylim');
    plot([param_median_aic param_median_aic], y_lim, 'r--', 'linewidth', 2);
    plot([param_median_opt param_median_opt], y_lim, 'g-.', 'linewidth', 2);
    plot([param_median_gt param_median_gt], y_lim, 'b--', 'linewidth', 2);
    plot([param_gt param_gt], y_lim, 'k-', 'linewidth', 3);
    set(gca, 'xlim', [0 param_lim+bar_width]);
    
    legend({
        'AIC voxel-wise predictions'
        'Best SSE voxel-wise predictions'
        'Correct model voxel-wise predictions'
        sprintf('AIC median (%4.3f)', param_median_aic)
        sprintf('Best SSE median (%4.3f)', param_median_opt)
        sprintf('Correct model median (%4.3f)', param_median_gt)
        sprintf('True value (%4.3f)', param_gt)});
    
    saveas(gcf, sprintf('%smodel_fit_results_n%d_m%d_p%s.fig',...
        analysis_dir, i_noise, i_gt, param));
    saveas(gcf, sprintf('%smodel_fit_results_n%d_m%d_p%s.png',...
        analysis_dir, i_noise, i_gt, param));
    close(gcf);
    %
    end
%% ***********************************************************************
    function plot_param_hists2(param, param_gt, param_lim, ...
            aic_model_idx, sse_model_idx)
    
    param_range = [0 param_lim];%[...
        %min(pred.aic.(param)(valid_aic)), max(pred.aic.(param)(valid_aic))];
    param_bins = linspace(param_range(1), param_range(2), 50);
    bar_width = param_bins(2)-param_bins(1);
    
    figure;
    if args.do_grid_search
        models = 1:3;
        plot_rows = 2;
        plot_cols = 2;
    else
        models = 1:9;
        plot_rows = 3;
        plot_cols = 3;
    end
    for model = models
        
        if isfield(pred, param)
            param_pred = pred.(param)(:,model);
        elseif isfield(pred.active, param)
            param_pred = pred.active.(param)(:,model);
        elseif isfield(pred.cxm, param)
            param_pred = pred.cxm.(param)(:,model);
        end
        
        aic_idx = aic_model_idx(:,model);
        sse_idx = sse_model_idx(:,model);
        
        param_median = nanmedian(param_pred);
        param_median_aic = nanmedian(param_pred(aic_idx));
        param_median_sse = nanmedian(param_pred(sse_idx));
        
        param_counts = hist(param_pred, param_bins); %#ok
        
        subplot(plot_rows,plot_cols,model);
        bar(param_bins, param_counts); hold all;
        
        y_lim = get(gca, 'ylim');
        plot([param_gt param_gt], y_lim, 'k-', 'linewidth', 1.5);
        plot([param_median param_median], y_lim, 'c--', 'linewidth', 1.5);
        plot([param_median_aic param_median_aic], y_lim, 'r--', 'linewidth', 1.5);
        plot([param_median_sse param_median_sse], y_lim, 'g--', 'linewidth', 1.5);
        set(gca, 'xlim', [0 param_lim+bar_width]);
    
        if model == 1
            legend({
                'Param distribution'
                sprintf('True value (%4.3f)', param_gt)
                sprintf('All voxels median (%4.3f)', param_median)
                sprintf('Lowest AIC median (%4.3f)', param_median_aic)
                sprintf('Best SSE median (%4.3f)', param_median_sse)
                });
        else
            legend({
                ''
                sprintf('(%4.3f)', param_gt)
                sprintf('(%4.3f)', param_median)
                sprintf('(%4.3f)', param_median_aic)
                sprintf('(%4.3f)', param_median_sse)
                });
        end
        
        title(sprintf('#AIC = %d, #SSE = %d', sum(aic_idx), sum(sse_idx)));
    end
    
    saveas(gcf, sprintf('%smodel_fit_results2_n%d_m%d_p%s.fig',...
        analysis_dir, i_noise, i_gt, param));
    saveas(gcf, sprintf('%smodel_fit_results2_n%d_m%d_p%s.png',...
        analysis_dir, i_noise, i_gt, param));
    close(gcf);
    %
    end

%% ***********************************************************************
    function noise_time_series = get_noise_time_series()
        noise_time_series = zeros(n_pts, n_t);
        curr_vox = 0;
        num_subs = length(args.subject_list);
        for i_sub = 1:num_subs
            subject_dir = [args.study_dir args.subject_list{i_sub} ...
                '/visit1/'];

            if i_sub < num_subs
                vox_idx = curr_vox + (1:round(n_pts/num_subs));
                curr_vox = vox_idx(end);
            else
                vox_idx = (curr_vox+1):n_pts;
            end
            n_vox = length(vox_idx);
            
            roi_mask = load_img_volume(...
                [subject_dir args.roi_mask]);
            for i_ero = 1:3
                roi_mask = imerode(...
                    roi_mask, strel('disk', 1));
            end
            roi_slices = squeeze(any(any(roi_mask,1),2));
            slice1 = find(roi_slices, 3, 'first');
            slice2 = find(roi_slices, 3, 'last');
            roi_mask(:,:,[slice1 slice2]) = 0;
            
            mask_voxels = find(roi_mask);
            rand_idx = randperm(length(roi_mask), n_vox);
            subset_mask = false(size(roi_mask));
            subset_mask(mask_voxels(rand_idx)) = 1;

            dyn_conc_dir = [subject_dir args.dyn_conc_dir '\'];
            model_conc_dir = [subject_dir args.model_conc_dir '\'];

            C_ti = get_dyn_vals(...
                [dyn_conc_dir 'Ct_sig'], n_t, subset_mask);
            C_t_mi = get_dyn_vals(...
                [model_conc_dir 'Ct_mod'], n_t, subset_mask);
            
            noise_time_series(vox_idx,:) = C_ti - C_t_mi;
        end
            
    end
%% ************************************************************************
end