%% DIBEM model framework, Monte-Carlo experiments
%% ------------------------------------------------------------------------
% Experiment 1: fixed parameters
% * Fixed parameters at 4 model forms
%   - Active uptake + efflux, 20% arterial
%   - Active uptake only, 20% arterial
%   - No uptake, 20% arterial (i.e. dual-input single compartment)
%   - 2CXM, 100% arterial
%
% * 4 levels of increasing noise
% * Noise sampled directly from patient data residuals
% * Fit 1000 voxels for each level, for each form
% * Repeat for DCE time-series durations 6, 12, 24 mins
% * Repeat 3.6s, 6s, 30s resolution
%
% * Analysis
%   - % of voxels showing active accumulation
%   - % of voxels selected for each model form, using
%       + Model residuals (SSE)
%       + AIC
%   - Median absolute error to each ground-truth parameter
%       + For each model form
%       + For voxels selected by SSE/AIC
%
% Experiment 2: varied
% * Real noise
% * Range of parameters sampled from patient data distributions
%   - 5000 voxels from accumulating liver ROI distributions in active+efflux form
%   - 5000 voxels from tumour ROI distributions in 2CXM form
% * Analysis
%   - How well do we predict parameters using model selection criteria?
experiment_root = 'Q:/data/MB/milano_primovist/experiments/monte_carlo/resubmission';
%% Experiment 1
for temp_res = [30.0 12.0 6.0 3.8]
    for end_time = [7 13 25]
        milano_monte_carlo_fixed(...
            'experiment_root', experiment_root,...
            'results_dir', 'fixed',...
            'analysis_dir', 'analysis',...
            'n_pts', 1e3,...
            'n_t', 0,...
            'temporal_resolution', temp_res,...
            'end_time', end_time,...
            'injection_image', 0,...
            'injection_time', 60,...
            'gt_idx', 4,...
            'do_fitting', 1,...
            'do_analysis', 0,...
            'dummy_run', 0);
    end
    for end_time = 25
        milano_monte_carlo_fixed(...
            'experiment_root', experiment_root,...
            'results_dir', 'fixed',...
            'analysis_dir', 'analysis',...
            'n_pts', 1e3,...
            'n_t', 0,...
            'temporal_resolution', temp_res,...
            'end_time', end_time,...
            'injection_image', 0,...
            'injection_time', 60,...
            'gt_idx', 1:3,...
            'do_fitting', 1,...
            'do_analysis', 0,...
            'dummy_run', 0);
    end
end
%%
old_root = 'Q:/data/MB/milano_primovist/experiments/monte_carlo';
for temp_res = [30.0 12.0 6.0 3.8]
    for end_time = [7 13]
        results_dir = sprintf('%s/%s/tr%2.1f_et%2.1f%s/',...
            experiment_root, 'fixed',...
            temp_res, end_time, '');
        old_dir = sprintf('%s/%s/tr%2.1f_et%2.1f%s/',...
            old_root, 'fixed',...
            temp_res, end_time, '');
        
        for i_m = 1:3
            for i_n = 1:4
                model_name = sprintf('model_fit_n%d_m%d.mat', i_n, i_m);
                copyfile([old_dir model_name], results_dir);
            end
        end
    end
end
%%
for temp_res = [30.0 12.0 6.0 ]
    for end_time = [7 13 25]
        milano_monte_carlo_fixed(...
            'experiment_root', experiment_root,...
            'results_dir', 'fixed',...
            'analysis_dir', 'analysis',...
            'n_pts', 1e3,...
            'n_t', 0,...
            'temporal_resolution', temp_res,...
            'end_time', end_time,...
            'injection_image', 0,...
            'injection_time', 60,...
            'gt_idx', 3,...%1:4,...
            'do_fitting', 0,...
            'do_analysis', 1,...
            'dummy_run', 0);
    end
end

%%
milano_monte_carlo_fixed(...
    'experiment_root', experiment_root,...
    'results_dir', 'fixed', ...
    'analysis_dir', 'analysis',...
    'n_t', 100,...
    'injection_image', 8,...
    'noise_idx', 1,...
    'use_models', 1:3,...
    'do_fitting', 1, ...
    'do_analysis', 1,...
    'use_M0', 0, ...
    'use_existing_data', 0,...
    'use_real_aif', 1,...
    'use_real_noise', 1,...
    'gt_idx', 4,...%1:4,...
    'study_dir', 'C:/isbe/qbi/data/milano_primovist/',...
    'subject_list', milano_subjects(),...
    'dyn_conc_dir', 'dynamic_conc_reg_ants',...
    'model_conc_dir', 'mdm_analysis_ants_DIBEM_f7',...
    'aif_name', 'PRIMDCE_1/visit1/mdm_analysis_T1/group_aif.txt',...
    'roi_mask', 'analysis_masks/liver_tumour_roi.hdr',...
    'dummy_run', 0);

%%
%**************************************************************************
%**************************************************************************
% Sandpit - experimental stuff below this line, no guarantee of working
%%
base_dir = 'Q:/data/MB/milano_primovist/experiments/monte_carlo/resubmission/';
fixed_dir = [base_dir 'fixed/'];

%%
model_forms = {'auem', 'aum', 'sc', '2cxm'};
selections = {'AIC', 'SSE'};
temp_res = [3.8 6.0 12.0 30.0];
end_time = [7 13 25];
%%
figs = cell(4,2);
for i_tr = 1:4
    for i_sel = 1:2
        figs{i_tr,i_sel} = ...
            figure('Name', sprintf('Temp res = %2.1f', temp_res(i_tr))); 
    end
end

%
for i_tr = 1:4
    for i_et = 1:3
        if i_tr == 1 && i_et == 1
            real_noise_dir = [fixed_dir 'tr3.8_et6.3_real_aif/'];
        else
            real_noise_dir = [];
        end
        for i_sel = 1:2
            plot_selections(fixed_dir, temp_res(i_tr), end_time(i_et),...
                selections{i_sel}, figs{i_tr,i_sel},...
                i_et, 'analysis', real_noise_dir);
        end        
    end
end
%%
fig_dir = [fixed_dir 'figs/'];
create_folder(fig_dir);
for i_tr = 1:4
    for i_sel = 1:2
        saveas(figs{i_tr,i_sel}, sprintf('%s/mc_fixed_tr%2.1f_%s.png',...
            fig_dir, temp_res(i_tr), selections{i_sel})); 
    end
end
%%
% ------------------------------------------------------------------------
gt_names = {'Active bi-exp', 'Active mono-exp', 'Active SC', '2CXM'};
figs = cell(4,6);

for i_tr = 1:4
    for i_p = 1:6
        figs{i_tr,i_p} = figure('Name', gt_names{i_tr}); 
    end
end
temp_res = [3.8 6.0 12.0 30.0];
end_time = [7 13 25];
%
for i_t = 1:4
    for i_et = 1:3
        if i_t == 1 && i_et == 1
            real_noise_dir = [fixed_dir 'tr3.8_et6.3_real_aif/'];
        else
            real_noise_dir = [];
        end
        plot_param_error(fixed_dir, temp_res(i_t), end_time(i_et),...
            figs, i_et, i_t, 'analysis', real_noise_dir, 1:4, 1:4, 0);
    end
end
%
fig_dir = [fixed_dir 'figs/'];
for i_tr = 1:4
    for i_p = 1:6
        saveas(figs{i_tr,i_p}, sprintf('%s/mc_fixed_%s_p%d.png',...
            fig_dir, model_forms{i_tr}, i_p)); 
    end
end

%%
% -------------------------------------------------------------------------
gt_names = {'Active bi-exp', 'Active mono-exp', 'Active SC', '2CXM'};
figs = cell(4,6);
for i_tr = 1:4
    for i_p = 1:6
        figs{i_tr,i_p} = figure('Name', gt_names{i_tr}); 
    end
end
temp_res = [3.8 6.0 12.0 30.0];
end_time = [7 13 25];
%
for i_t = 1:4
    for i_et = 1:3
        if i_t == 1 && i_et == 1
            real_noise_dir = [fixed_dir 'tr3.8_et6.3_real_aif/'];
        else
            real_noise_dir = [];
        end
        plot_param_error_single(fixed_dir, temp_res(i_t), end_time(i_et),...
            figs, i_et, i_t, 'analysis', real_noise_dir, 1:4, 2, 1:6);
    end
end
%%
fig_dir = [fixed_dir 'figs/'];
for i_tr = 1:4
    for i_p = 1:6
        saveas(figs{i_tr,i_p}, sprintf('%s/mc_fixed_single_%s_p%d.png',...
            fig_dir, model_forms{i_tr}, i_p)); 
    end
end
%%
%--------------------------------------------------------------------------
%%
function plot_selections(fixed_dir, temp_res, end_time, ...
    count_type, fig, plot_row, analysis_name, real_noise_dir)

    gt_names = {'AUEM', 'AUM', 'SCM', '2CXM'};
    
    experiment_dir = sprintf('%s/tr%2.1f_et%2.1f/',...
            fixed_dir, temp_res, end_time);
    analysis_dir =sprintf('%s%s/', experiment_dir, analysis_name);
    fprintf('%s: %d\n', analysis_dir, exist(analysis_dir, 'dir'));
    
    marker_type = {'ro', 'go', 'bo', 'ks'};
    line_type = {'r-', 'g-', 'b-', 'k--'};
    
    switch count_type
        case 'AIC'
            count_field = 'aic_model_idx';
            legend_str = {'IRF_2', 'IRF_3', 'IRF_4', 'Active'};
            n_m = 4;
        case 'SSE'
            count_field = 'sse_model_idx';
            legend_str = {'IRF_2', 'IRF_3', 'IRF_4'};
            n_m = 3;
        case 'Accumulation'
            count_field = 'accum_idx';
            legend_str = {'IRF_2', 'IRF_3', 'IRF_4'};
            n_m = 3;
        otherwise
            return;
    end

    figure(fig);
    
    %Loop through each ground-truth model form
    for i_gt = 1:4
        plot_col = i_gt;
        
        %Compute model selection counts for each noise level
        model_counts = zeros(4, 4);
        snr = zeros(4,1);
        start_t = round(2*60/temp_res);
        end_t = floor(6*60/temp_res);
        for i_n = 1:4
            s = load(sprintf('%smodel_fit_results_n%d_m%d.mat',...
                    analysis_dir, i_n, i_gt));
            c = load(sprintf('%smodel_fit_n%d_m%d.mat',...
                    experiment_dir, i_n, i_gt), 'C_t_n');
            model_counts(i_n,1:3) = sum(squeeze(s.(count_field))) / 10;
            model_counts(i_n,4) = sum(squeeze(s.active_idx)) / 10;
            
            tse = compute_time_series_error(c.C_t_n, start_t, end_t);
            mean_signal = mean2(c.C_t_n(:,start_t:end_t));
            snr(i_n) = mean(sqrt(tse)) ./ mean_signal;
        end
        %
        
        subplot(3,4,4*(plot_row-1)+plot_col); hold all;
        for i_m = 1:n_m
            plot(snr, model_counts(:,i_m),...
                line_type{i_m}, 'linewidth', 3);
        end
        if plot_row== 1
            if ~isempty(real_noise_dir)
                s = load(sprintf('%s%s/model_fit_results_n1_m%d.mat',...
                    real_noise_dir, analysis_name, i_gt));
                c = load(sprintf('%smodel_fit_n1_m%d.mat',...
                    real_noise_dir, i_gt));

                tse = compute_time_series_error(c.C_t_n, start_t, end_t);
                mean_signal = mean2(c.C_t_n(:,start_t:end_t));
                snr_real = mean(sqrt(tse)) ./ mean_signal;

                real_counts = [sum(squeeze(s.(count_field))) /  10 ...
                    sum(squeeze(s.active_idx)) / 10];
                for i_m = 1:n_m
                    plot(snr_real, real_counts(i_m),...
                        marker_type{i_m}, ...
                        'markerfacecolor', marker_type{i_m}(1),...
                        'markersize', 10);
                end
            end
        end
        
        if plot_row== 3 && plot_col == 4
            legend(legend_str, 'location', 'sw', 'autoupdate', 'off');
        end
                     
        if plot_row == 1
            title(gt_names{i_gt});
        elseif plot_row == 3
            xlabel('SNR^{-1}'); 
        end
        if plot_col == 1
            ylabel({sprintf('{\\bf %1.0f mins}', end_time-1), '% of samples'});
        end
        set(gca, 'ylim', [0 100], 'fontsize', 14);
    end

end
%%
function plot_param_error(fixed_dir, temp_res, end_time, ...
    figs, plot_row, plot_col, analysis_name, real_noise_dir,...
    gt_idx, noise_idx, irf2_2cxm)

    experiment_dir = sprintf('%s/tr%2.1f_et%2.1f/',...
            fixed_dir, temp_res, end_time);
    analysis_dir =sprintf('%s%s/', experiment_dir, analysis_name);
        
    legend_str = {'IRF_2', 'IRF_3', 'IRF_4', 'AIC', 'SSE', 'Real'};
    legend_objs = zeros(6, 1);
    line_color = {'r', 'g', 'b', 'm', 'm'};
    face_color = {'r', 'g', 'b', 'm', 'w'};
    
    F_p_active = 0.5;
    f_a_active = 0.2;
    tau_a_active = 0.1;
    v_ecs_active = 0.15;
    k_i_active = 0.07;
    k_ef_active = 0.05;
    %A3: Active, no uptake
    F_p_active_lo = 0.15;
    v_ecs_active_hi = 0.5;

    %X1: 2CXM
    F_p_cxm = 0.4;
    f_a_cxm = 0.6;
    tau_a_cxm = 0.05;
    PS_cxm = 0.05;
    v_e_cxm = 0.12;
    v_p_cxm = 0.10;
    param_gt = [...
        F_p_active f_a_active tau_a_active v_ecs_active k_i_active k_ef_active;
        F_p_active f_a_active tau_a_active v_ecs_active k_i_active 0;
        F_p_active_lo f_a_active tau_a_active v_ecs_active_hi 0 0;
        F_p_cxm f_a_cxm tau_a_cxm PS_cxm v_e_cxm v_p_cxm];
    
    if irf2_2cxm
        param_names = {
                {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
                {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
                {'F_p' 'f_a' '\tau_a' 'v_e' 'PS' 'v_p'}
                {'F_p' 'f_a' '\tau_a' 'PS' 'v_e' 'v_p'}
            }; 
        param_limit = [...
            1.0 1.1 0.5 1.0 0.1 0.1;
            1.0 1.1 0.5 1.0 0.1 0.1;
            1.0 1.1 0.5 1.0 0.1 1.0;          
            1.0 1.1 0.3 0.1 0.5 0.2];
    else
        param_names = {
                {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
                {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
                {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
                {'F_p' 'f_a' '\tau_a' 'PS' 'v_e' 'v_p'}
            };
        param_limit = [...
            1.0 1.1 0.5 1.0 0.1 0.1;
            1.0 1.1 0.5 1.0 0.1 0.1;
            1.0 1.1 0.5 1.0 1.0 0.3;
            1.0 1.1 0.3 0.1 0.5 0.2];
    end  
 
    for i_gt = gt_idx

        param_med = zeros(max(noise_idx), 5, 6);
        param_iqr = zeros(2, max(noise_idx), 5, 6);
        snr = zeros(max(noise_idx), 1);
        start_t = round(2*60/temp_res);
        end_t = floor(6*60/temp_res);
        
        for i_n = noise_idx
            s = load(sprintf('%smodel_fit_results_n%d_m%d.mat',...
                analysis_dir, i_n, i_gt));
            c = load(sprintf('%smodel_fit_n%d_m%d.mat',...
                    experiment_dir, i_n, i_gt), 'C_t_n');
            tse = compute_time_series_error(c.C_t_n, start_t, end_t);
            mean_signal = mean2(c.C_t_n(:,start_t:end_t));
            snr(i_n) = mean(sqrt(tse)) ./ mean_signal;
            
            for i_p = 1:6
                for i_m = 1:3
                    param_med(i_n,i_m,i_p) = ...
                        s.param_analysis{i_m,i_p}.all.median;
                    param_iqr(:,i_n,i_m,i_p) = ...
                        s.param_analysis{i_m,i_p}.all.q25_75;
                end

                param_med(i_n,4,i_p) = ...
                    s.param_analysis{10,i_p}.all.median;
                param_med(i_n,5,i_p) = ...
                    s.param_analysis{11,i_p}.all.median;
                
                param_iqr(:,i_n,4,i_p) = ...
                    s.param_analysis{10,i_p}.all.q25_75;
                param_iqr(:,i_n,5,i_p) = ...
                    s.param_analysis{11,i_p}.all.q25_75;
            end
        end
        %
        
        
        snr_width = min(diff(snr));
        offsets = snr_width*[-2 -1 0 1 2]/8;             
        for i_p = 1:6
            figure(figs{i_gt, i_p});           
            subplot(3,4,4*(plot_row-1)+plot_col); hold all;
            plot([0 max(snr)+snr_width], param_gt(i_gt, i_p)*[1 1], 'k--');
            for i_m = 1:5
                for i_n = noise_idx
                    plot(snr(i_n)*[1 1]+offsets(i_m), ...
                        param_iqr(:,i_n,i_m,i_p), ...
                        [line_color{i_m} '-'], 'linewidth', 2);
                    
                    legend_objs(i_m) = plot(...
                        snr(i_n) + offsets(i_m), param_med(i_n,i_m,i_p), ...
                        [line_color{i_m} 'o'], ...
                        'markerfacecolor', face_color{i_m},...
                        'markersize', 8);                   
                end
                
            end
            if plot_row*plot_col == 1
                if ~isempty(real_noise_dir)
                    s = load(sprintf('%s%s/model_fit_results_n1_m%d.mat',...
                        real_noise_dir, analysis_name, i_gt));
                    c = load(sprintf('%smodel_fit_n1_m%d.mat',...
                        real_noise_dir, i_gt));

                    tse = compute_time_series_error(c.C_t_n, start_t, end_t);
                    mean_signal = mean2(c.C_t_n(:,start_t:end_t));
                    snr_real = mean(sqrt(tse)) ./ mean_signal;
                    
                    %param_med_real_aic = ...
                    %    s.param_analysis{10,i_p}.all.median;
                    param_med_real_sse = ...
                        s.param_analysis{11,i_p}.all.median;
                
                    %param_iqr_real_aic = ...
                    %    s.param_analysis{10,i_p}.all.q25_75;
                    param_iqr_real_sse = ...
                        s.param_analysis{11,i_p}.all.q25_75;
                    
                    %plot(snr_real*[1 1]+offsets(2), param_iqr_real_aic, ...
                    %    'k-', 'linewidth', 2);
                    plot(snr_real*[1 1], param_iqr_real_sse, ...
                        'k-', 'linewidth', 2);
                    
                    %plot(snr_real+offsets(2), param_med_real_aic, ...
                    %    'kd', ...
                    %    'markerfacecolor', 'c',...
                    %    'markersize', 8);
                    legend_objs(6) = plot(snr_real, param_med_real_sse, ...
                        'kd', ...
                        'markerfacecolor', 'c',...
                        'markersize', 10);
                end
            end

            xlim([0 max(snr)+snr_width]);

            if plot_row == 1
                title(sprintf('\\delta = %2.1fs', temp_res));
            elseif plot_row == 3
                xlabel('SNR^{-1}'); 
            end
            if plot_col == 1
                ylabel({sprintf('{\\bf %1.0f mins}', end_time-1);...
                    sprintf('{\\it %s}', param_names{i_gt}{i_p})});
            end
            set(gca, 'ylim', [0 param_limit(i_gt,i_p)], 'fontsize', 14);
            
%             if plot_row * plot_col == 1
%                 legend(legend_objs, legend_str, ...
%                     'NumColumns',2,'FontSize', 8,...
%                     'location', 'northwest');
%             end
        end
    end
end
%%
function plot_param_error_single(fixed_dir, temp_res, end_time, ...
    figs, plot_x, plot_num, analysis_name, real_noise_dir,...
    gt_idx, noise_idx, params)

    experiment_dir = sprintf('%s/tr%2.1f_et%2.1f/',...
            fixed_dir, temp_res, end_time);
    analysis_dir =sprintf('%s%s/', experiment_dir, analysis_name);
        
    line_color = {'r', 'g', 'b', 'm', 'm'};
    face_color = {'r', 'g', 'b', 'm', 'w'};
    
    if plot_x == 1 && plot_num == 1
        legend_str = {'IRF_2', 'IRF_3', 'IRF_4', 'AIC', 'SSE', 'Real'};
        legend_objs = zeros(6, 1);
    end
    
    F_p_active = 0.5;
    f_a_active = 0.2;
    tau_a_active = 0.1;
    v_ecs_active = 0.15;
    k_i_active = 0.07;
    k_ef_active = 0.05;
    %A3: Active, no uptake
    F_p_active_lo = 0.15;
    v_ecs_active_hi = 0.5;

    %X1: 2CXM
    F_p_cxm = 0.4;
    f_a_cxm = 1.0;
    tau_a_cxm = 0.05;
    PS_cxm = 0.05;
    v_e_cxm = 0.12;
    v_p_cxm = 0.10;
    param_gt = [...
        F_p_active f_a_active tau_a_active v_ecs_active k_i_active k_ef_active;
        F_p_active f_a_active tau_a_active v_ecs_active k_i_active 0;
        F_p_active_lo f_a_active tau_a_active v_ecs_active_hi 0 0;
        F_p_cxm f_a_cxm tau_a_cxm PS_cxm v_e_cxm v_p_cxm];
    
    param_names = {
            {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
            {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
            {'F_p' 'f_a' '\tau_a' 'v_{ecs}' 'k_i' 'k_{ef}'}
            {'F_p' 'f_a' '\tau_a' 'PS' 'v_e' 'v_p'}
        };
    param_limit = [...
        1.0 1.1 0.5 1.0 0.1 0.1;
        1.0 1.1 0.5 1.0 0.1 0.1;
        1.0 1.1 0.5 1.0 1.0 0.3;
        1.0 1.1 0.3 0.1 0.5 0.2];  
 
    for i_gt = gt_idx

        param_med = zeros(5, 6);
        param_iqr = zeros(2, 5, 6);
        
        s = load(sprintf('%smodel_fit_results_n%d_m%d.mat',...
            analysis_dir, noise_idx, i_gt));
            
        for i_p = params
            for i_m = 1:3
                param_med(i_m,i_p) = ...
                    s.param_analysis{i_m,i_p}.all.median;
                param_iqr(:,i_m,i_p) = ...
                    s.param_analysis{i_m,i_p}.all.q25_75;
            end

            param_med(4,i_p) = ...
                s.param_analysis{10,i_p}.all.median;
            param_med(5,i_p) = ...
                s.param_analysis{11,i_p}.all.median;

            param_iqr(:,4,i_p) = ...
                s.param_analysis{10,i_p}.all.q25_75;
            param_iqr(:,5,i_p) = ...
                s.param_analysis{11,i_p}.all.q25_75;
        end
        
        offsets = [-2 -1 0 1 2 3]/8;             
        for i_p = params
            figure(figs{i_gt, i_p});           
            subplot(2 , 2, plot_num); hold all;
            
            if plot_x==1
                plot([0 4], param_gt(i_gt, i_p)*[1 1], 'k--');
            end
                
            for i_m = 1:5
                plot(plot_x*[1 1] + offsets(i_m), ...
                    param_iqr(:,i_m,i_p), ...
                    [line_color{i_m} '-'], 'linewidth', 2);
                h = plot(...
                    plot_x + offsets(i_m), param_med(i_m,i_p), ...
                    [line_color{i_m} 'o'], ...
                    'markerfacecolor', face_color{i_m},...
                    'markersize', 8); 
                    
                if plot_x==1 && plot_num == 1
                    legend_objs(i_m) = h;
                end
                
            end
            if plot_x==1 && plot_num == 1
                s = load(sprintf('%s%s/model_fit_results_n1_m%d.mat',...
                    real_noise_dir, analysis_name, i_gt));

                param_med_real_sse = ...
                    s.param_analysis{11,i_p}.all.median;
                param_iqr_real_sse = ...
                    s.param_analysis{11,i_p}.all.q25_75;
                plot(plot_x*[1 1] + offsets(6), param_iqr_real_sse, ...
                    'k-', 'linewidth', 2);

                legend_objs(6) = plot(...
                    plot_x + offsets(6), param_med_real_sse, ...
                    'kd', ...
                    'markerfacecolor', 'c',...
                    'markersize', 10);
            end
            
            if plot_x == 1
                title(sprintf('\\delta = %2.1fs', temp_res)); 
                
                if plot_num > 2
                    xlabel('Duration (mins)');
                end
                if ismember(plot_num, [1 3])
                    ylabel(sprintf('{\\it %s}', param_names{i_gt}{i_p}));
                end
                if plot_num == 1
                    legend(legend_objs, legend_str, ...
                        'NumColumns',2,'FontSize', 14,...
                        'location', 'northeast',...
                        'AutoUpdate', 'off');
                end
                
            end          
            if plot_x == 3
                set(gca, ...
                    'xlim', [0 4],...
                    'xtick', 1:3,...
                    'xticklabel', [6 12 24],...
                    'ylim', [0 param_limit(i_gt,i_p)], 'fontsize', 16);
            end               
        end
    end
end


    


